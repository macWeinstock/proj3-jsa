### Project 3

- Author: Mac Weinstock

- Contact: mweinsto@uoregon.edu

- Project3: Created a vocabulary game with flask backend and AJAX+Javascript front end. Find three words in the list to win, otherwise receive a corresponding message sent from the server.

*TODO: Make messages display in a more standard way. Error messages disappear after input is erased and new input is entered. Found words appear alone underneath error messages.*